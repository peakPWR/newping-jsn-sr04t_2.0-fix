# NewPing JSN-SR04T_2.0 FIX

**FIX: NewPing lib**
* FIX FOR JSN-SR04T-2.0 (VERSION 2 Board), which has some stability issues when Triggertime/EchoReadtime is only 4/10 (NewPing.cpp Line 134 and 137 changed).

**What was changed:**
* The v2.0 Boards tend to be unstable when the trigger pin is only set to HIGH (send a ping signal) for 10µS (which should be fine according to the datasheet, but isn't). To fix this issue the time the trigger pin is set HIGH was increased to 13µS so it is ensured the sensor receives the trigger signal.


**Credits:**
* Based on the original NewPing Library v1.8 - 07/30/2016 by Tim Eckel (teckel@leethost.com) licensed under the GNU GPL v3 (http://www.gnu.org/licenses/gpl.html)
