/*
 * Example to interface 8 sensors (JSN-Sr04t-2.0) with one Arduino Nano
 * based on NewPing Library v1.8 by Tim Eckel
 * edited by peakPWR
 */
 
// Don't use <...> as it would load lib from Arduino IDEs Path; use NewPing.h in same PATH instead by wrapping the lib name with quotes: "..."
//#include <NewPing.h> 
#include "NewPing.h" 

#define ONE_PIN_ENABLED false

#define SONAR_NUM 8      // number of sensors
#define MAX_DISTANCE 200 // max. distance in cm


// Pin numbers of Arduino Nano
// NewPing(trigger pin, echo pin,max distance to ping)
NewPing sonar[SONAR_NUM] = {
  NewPing(2, 3, MAX_DISTANCE),
  NewPing(4, 5, MAX_DISTANCE), 
  NewPing(6, 7, MAX_DISTANCE),
  NewPing(8, 9, MAX_DISTANCE),
  NewPing(10, 11, MAX_DISTANCE),
  NewPing(12, 13, MAX_DISTANCE),
  NewPing(A0, A1, MAX_DISTANCE),  // use Analog Pins as Digital ones
  NewPing(A2, A3, MAX_DISTANCE)   // use Analog Pins as Digital ones
};

void setup() {
  Serial.begin(115200); // set serial to 115200 baud
}

void loop() { 
  for (uint8_t i = 0; i < SONAR_NUM; i++) {
    delay(50);
    Serial.print(i);
    Serial.print("=");
    Serial.print(sonar[i].ping_cm());
    Serial.print("cm ");
  }
  Serial.println();
}
